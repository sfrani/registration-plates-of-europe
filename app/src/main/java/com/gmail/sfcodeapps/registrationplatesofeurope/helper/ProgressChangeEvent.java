package com.gmail.sfcodeapps.registrationplatesofeurope.helper;

public class ProgressChangeEvent {

    public int progress;

    public ProgressChangeEvent(int progress){
        this.progress = progress;
    }

    public int getProgress(){
        return progress;
    }
}
